class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;        
    }

    get nameProgrammer() {
        return `${this.name}`;
    }

    get ageProgrammer() {
        return `${this.age}`;
    }

    get salaryProgrammer() {
        return `${this.salary}`;
    }

    set nameProgrammer(name) {
        this.name = name;
    }

    set ageProgrammer(age) {
        this.age = age;
    }

    set salaryProgrammer(salary) {
        this.salary = salary;
    }
}

// extends
class Programmer extends Employee {
    constructor(name, age, salary, ...lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get programmerAddSalary() {
        return `${(this.salary * 3)}`;
    }

}

const Developer1 = new Programmer("Maxis", 35, 1000, ['js', 'java']);
const Developer2 = new Programmer("Olena", 45, 1200, ['python', 'js']);
const Developer3 = new Programmer("Platon", 30, 1500, ['js', 'c++', 'python']);

console.log(Developer1);
console.log(Developer2);
console.log(Developer3);