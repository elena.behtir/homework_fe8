const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];


  function filterList(books) {
    const container = document.getElementById('root');
    let list = document.createElement('ul');
    books.forEach((i) => {   
        try {
            if (("author" in i) && ("name" in i) && ("price" in i)) {
                let li = document.createElement('li');
                li.textContent = JSON.stringify(i);
                list.append(li); 
                container.append(list);
            } else {
              throw new Error ("error");              
            }
        } catch (error) {
             if (!("author" in i)) {
                    console.log(`Error! The object ${JSON.stringify(i)} does not have the property "author"`);
                } else {
                    if (!("name" in i)) {
                        console.log(`Error! The object ${JSON.stringify(i)} does not have the property "name"`);
                    } else {
                        console.log(`Error! The object ${JSON.stringify(i)} does not have the property "price"`);
                    }
                }
            }
        })
    }
  
  filterList(books);