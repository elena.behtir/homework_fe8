// Завдання 1
// Дві компанії вирішили об'єднатись, і для цього їм потрібно об'єднати базу даних своїх клієнтів.
// У вас є 2 масиви рядків, у кожному з них – прізвища клієнтів. Створіть на їх основі один масив, який буде об'єднання двох масивів без повторюваних прізвищ клієнтів.

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const allClients = [...new Set([...clients1, ...clients2])]; // об'єднаний масив без повторюваних прізвищ
console.log(allClients);



//Завдання 2
// Перед вами массив characters, що складається з об'єктів. Кожен об'єкт описує одного персонажа.
// Створіть на його основі масив charactersShortInfo, що складається з об'єктів, у яких є тільки 3 поля - ім'я, прізвище та вік.

const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];

const charactersShortInfo = characters.reduce((acc, { name, lastName, age }) => { // Створення масиву, charactersShortInfo, що складається з об'єктів, у яких є тільки 3 поля
  acc.push({ name, lastName, age });
  return acc;
}, []);

console.log(charactersShortInfo);



// Завдання 3
// У нас є об'єкт' user. Напишіть деструктуруюче присвоєння, яке:
// властивість name присвоїть в змінну ім'я
// властивість years присвоїть в змінну вік
// властивість isAdmin присвоює в змінну isAdmin false, якщо такої властивості немає в об'єкті
// Виведіть змінні на екран.
 
const user1 = {
  name: "John",
  years: 30
};

let { name, years: age, isAdmin = false } = user1;
// додавання змінних на сторінку для їх показу на екрані
let div = document.createElement('div');
div.innerText = `${name}, ${age}, ${isAdmin}`;
document.body.append(div);

// виведення змінних на екран через модальне вікно
alert(name); // John
alert(age); // 30
alert(isAdmin); // false
