// Завдання 6
// Даний об'єкт employee. Додайте до нього властивості age і salary, не змінюючи початковий об'єкт (має бути створено новий об'єкт, який включатиме всі необхідні властивості). Виведіть новий об'єкт у консоль.

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }

const cloneEmployee = { ...employee, age: 51, salary: 80000 };
console.log(cloneEmployee);



// Завдання 7
// Доповніть код так, щоб він коректно працював


const array = ['value', () => 'showValue'];

// Допишіть код тут
const [value, showValue] = array; // дописаний код

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'