import { NavLink } from 'react-router-dom';
import './header.scss'

export function Header() {
    return (
        <header>
            <nav>
                <NavLink
                    to="/"
                    className=
                    {({ isActive }) =>
                        isActive ? 'active-link' : 'link'
                    }
                >
                    Головна сторінка
                </NavLink>
                <NavLink
                    to="/basket"
                    className={({ isActive }) =>
                        isActive ? 'active-link' : 'link'
                    }
                >
                    Корзина
                </NavLink>
                <NavLink
                    to="/favorites"
                    className={({ isActive }) =>
                        isActive ? 'active-link' : 'link'
                    }
                >
                    Обрані товари
                </NavLink>
            </nav>
            <div className="header">
                <img className="header__logo" src="./img/logo.jpg" alt="logo" />
                <h2 className="header__title">Інтернет-магазин "Оптичні прилади"</h2>
                {/* <div>
                    <div className='header__icons'>
                    {isModalBasket && <Basket cardList={props.cardList} 
                        totalCount = {props.countTotalPrice()}
                        onClose={() => {setModalBasket(false)}} />}
                        <i className="fa-solid fa-basket-shopping icons-header" 
                            onClick={(e) => {
                                setModalBasket(true)
                                // const cardList = props.cardList
                                // console.log(cardList)
                            }}
                        ></i>
                        <div>{props.cardList.length}</div>
                    </div>
                    
                    <div className='header__icons'>
                    {isModalSelect && <Select favoritList={props.favoritList}
                        onClose={() => {setModalSelect(false)}} />}
                        <i className="fa-solid fa-star icons-header"
                            onClick={(e) => {
                                setModalSelect(true)
                                // const favoritList = props.favoritList
                                // console.log(favoritList)
                            }}
                        ></i>
                        <div>{props.favoritList.length}</div>
                    </div>                     */}
                {/* </div> */}
                
            </div>
        </header>
    )
}

