export function Product({ onAddFavoritesClick, product, isFavorite, onAddToBasketClick }) {
    return (
        <div>
            <span>{product.name}</span>
            <span>{product.price}</span>
            {onAddFavoritesClick && <button
                onClick={() => onAddFavoritesClick(product.id)}
            >
                {isFavorite ? 'Remove from favorites' : 'Add to favorites'}
            </button>}
            {onAddToBasketClick && <button
                onClick={() => onAddToBasketClick(product.id)}
            >
                Add product to basket
            </button>}
        </div>
    )
}