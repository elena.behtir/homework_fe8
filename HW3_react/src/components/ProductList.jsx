import { Product } from './Product';

export function ProductList({ products, onProductAddToFavoriteClick, onProductAddToBasketClick }) {
    return (
        <div>
            {products.map(product => (
                <Product
                    key={product.id}
                    product={product}
                    isFavorite={product.isFavorite}
                    onAddFavoritesClick={onProductAddToFavoriteClick}
                    onAddToBasketClick={onProductAddToBasketClick}
                />
            ))}
        </div>
    )
}
