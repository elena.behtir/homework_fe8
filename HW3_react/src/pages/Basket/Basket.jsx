import React from 'react'
import { useOutletContext } from 'react-router-dom'
import { Product } from '../../components/Product';

export function Basket() {
    const { cardList, cards } = useOutletContext();

    return (
        <div>

           <div>
            {Object.entries(cardList).map(([productId, quantity]) => (
                <div key={productId}>
                    <Product
                     product={cards.find(product => product.id === productId)}
                    //  onAddFavoritesClick={() => {}}
                    //  isFavorite={false}
                    //  onAddToBasketClick={() => {}}
                      />
                      {/* <span>Qantity: {quantity}</span> */}
                </div>
            ))}
           </div>
        </div>
    )
}