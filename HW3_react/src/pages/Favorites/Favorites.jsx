import React from 'react'
import { useOutletContext } from 'react-router-dom'
import { ProductList } from '../../components/ProductList';

export function Favorites() {
    const { products, favorites, setFavorites } = useOutletContext();

    const productList = products
        .filter(product => favorites.includes(product.id))
        .map((product) => ({
            ...product,
            isFavorite: true
        }))

    const handleAddFavoritesClick = (productId) => {
        if (favorites.includes(productId)) {
            setFavorites(state => state.filter(fav => fav !== productId))
        } else {
            setFavorites(state => [...state, productId])
        }
    }

    return (
        <div>
            <span>FAVORITES</span>
            <div>
                <ProductList
                    products={productList}
                    onProductAddToFavoriteClick={handleAddFavoritesClick}
                />
            </div>
        </div>
    )
}