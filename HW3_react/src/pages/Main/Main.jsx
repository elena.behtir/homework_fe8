import React, { useState } from 'react'
import { Outlet } from 'react-router-dom';
import { Header } from '../../components/Header/Header';
import { Footer } from '../../components/Footer/Footer'
import { useEffect } from 'react';

// const savedFavorites = localStorage.getItem('favorites') || [];

export function Main() {

    const [cards, setCards] = useState([]);
    const [cardList, setCardList] = useState([]);
    const [favoritList, setFavoritList] = useState([]);
    
    
    useEffect(() => {
        fetch('./data/products.json', { method: 'GET' })
            .then(r => r.json())
            .then(data => {
                (setCards(data));
                console.log("data", data);
            })
            .catch(error => console.log('error'));
    }, [])

    // const [products, setProducts] = useState(myProducts);
    // const [favorites, setFavorites] = useState(savedFavorites);
    // const [basket, setBasket] = useState({})


    // useEffect(() => {
    //     localStorage.setItem('favorites', favorites);
    // }, [favorites])

    return (
        <div>
            <Header />
            <Outlet context={{
                favoritList,
                setFavoritList,
                cardList,
                setCardList,
                cards,
                setCards
            }} />
            <Footer />
        </div>
    )
}