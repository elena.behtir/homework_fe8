import { useOutletContext } from 'react-router-dom'
import { ProductList } from '../../components/ProductList';

export function Products() {
    const { products, favorites, setFavorites, basket, setBasket } = useOutletContext();

    const handleAddFavoritesClick = (productId) => {
        if (favorites.includes(productId)) {
            setFavorites(state => state.filter(fav => fav !== productId))
        } else {
            setFavorites(state => [...state, productId])
        }
    }

    const productList = products.map(product => ({
        ...product,
        isFavorite: favorites.includes(product.id)
    }))

    const handleAddToBasketClick = (productId) => {
        if (basket[productId]) {
            setBasket({
                ...basket,
                [productId]: basket[productId] + 1
            })
        } else {
            setBasket({
                ...basket,
                [productId]: 1
            })
        }
    }

    return (
        <ProductList
            products={productList}
            onProductAddToFavoriteClick={handleAddFavoritesClick}
            onProductAddToBasketClick={handleAddToBasketClick}
        />
    )
}
