import { createBrowserRouter } from "react-router-dom";
import { Basket } from "./pages/Basket/Basket";
import { Favorites } from "./pages/Favorites/Favorites";
import { Main } from "./pages/Main/Main";
import { Products } from "./pages/Main/Products";

export const router = createBrowserRouter([{
    path: '/',
    element: <Main />,
    children: [
        {
            path: '/',
            element: <Products />
        }, {
            path: '/basket',
            element: <Basket />
        }, {
            path: '/favorites',
            element: <Favorites />
        }
    ]
}])
