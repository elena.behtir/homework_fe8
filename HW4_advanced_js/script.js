// функція для виводу списку імен персонажів кожного фільму
function getCharacters(url) {
    // запит, для отримання інформації по кожному фільму
    fetch(url)
        .then(response => response.json())
        .then(data => {
            console.log(data.name);
            // перевірка, чи вже виведені на екран персонажі якого фільму, якщо так, то видаляємо та завантажуємо за новим запитом
            const prevFilm = document.querySelector('.prev-film');
            if (prevFilm) {
                prevFilm.innerHTML = '';
                prevFilm.classList.remove('prev-film');
            }
            const divFilm = document.createElement('div');
            divFilm.classList.add('prev-film');
            // виводимо назву фільму, на який клікнули для відображення його персонажів
            // додавання анімації завантаження та назви фільму на сторінку
            divFilm.innerText = data.name;
            const divAnimetion = document.createElement('div');
            divAnimetion.innerHTML = '<div class="container"><div class="yellow"></div><div class="red"></div><div class="blue"></div><div class="violet"></div></div>';
            document.body.prepend(divAnimetion);
            document.body.prepend(divFilm);
            // console.log(data.characters);
            // створення списку персонажів фільму
            const urlChars = data.characters;
            const ul = document.createElement('ul');
            urlChars.forEach((character) => {
                //запит для отримання інформації по кожному персонажу, якогось окремого фільма
                fetch(character)
                    .then(response => response.json())
                    .then(result => {
                        // console.log(result.name);
                        const liChar = document.createElement('li');
                        liChar.innerText = result.name;
                        ul.appendChild(liChar);
                    });
                    divFilm.appendChild(ul);
                    // прибираємо анімацію завантаження через певний проміжок часу
                    setTimeout (() => {
                        divAnimetion.remove();
                    }, 1500);
                });        
        });
}

// функція, яка формує список фільмів та виводить у цьому списку номер епізоду, назву фільму та його короткий зміст
function renderFilms(films) {
    const ol = document.createElement('ol');
    films.forEach(({episodeId, name, openingCrawl, url}) => {
        const liFilm = document.createElement('li');
        liFilm.innerText = `episodeId - ${episodeId}, name - ${name}, openingCrawl - ${openingCrawl}`;
        // формуємо лінк на назву фільма для переходу до детальної інформації про персонажів конкретного фільму
        const a = document.createElement('a');
        a.setAttribute('href', "#");
        a.innerText = name;
        liFilm.appendChild(a);
        ol.appendChild(liFilm);
        a.addEventListener("click", () => getCharacters(url))
        });
        document.body.appendChild(ol);
    };

// запит на сервер для отримання інформації про фільми серіалу "Зоряні війни"
fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(data => {
        console.log(data);
        renderFilms(data);
    });