class Card {
    constructor (name, username, email, title, body) {
        this.name = name,
        this.username = username,
        this.email = email,
        this.title = title,
        this.text = body,
        this.postId = id
    }

    renderCards() {
        this.addUser = document.createElement('div');
        this.addUser.classList.add('new-user');
        const addName = document.createElement('div');
        const addUsername = document.createElement('div');
        const addEmail = document.createElement('div');
        const addTitle = document.createElement('div');
        const addText = document.createElement('div');
        addName.innerText = this.name;
        addName.style.fontWeight = '700';
        addUsername.innerText = this.username;
        addEmail.innerText = this.email;
        addEmail.style.color = 'grey';        
        addTitle.innerText = this.title;
        addTitle.classList.add('title');
        addText.innerText = this.text;
        addText.classList.add('text');
        const deleteCard = document.createElement('div');
        deleteCard.innerHTML = '<img class="btn-delete" src="./img/trash.png" alt="#">';
        const id = this.postId;
        // console.log(id);
        deleteCard.addEventListener('click', () => {
            (async() => {
                const result = await fetch (`https://ajax.test-danit.com/api/json/posts/${id}`, {
                    method: 'DELETE',
                }).then(response => {
                    console.log(response);
                    this.addUser.remove(id);
                });
            })();           
        });
        this.addUser.append(addName, addUsername, addEmail, addTitle, addText, deleteCard);
        const rootCard = document.querySelector('.root');
        rootCard.prepend(this.addUser);  
    }     
}

class Api {
    addLoader() {
        const loader = document.createElement('div');
        loader.classList.add('loader');
        loader.innerHTML = '<span></span>'
        document.body.appendChild(loader);
    }
    async getUsers() {
        return await fetch ('https://ajax.test-danit.com/api/json/users').then(response => response.json())
    }
    async getPosts() {
        return await fetch ('https://ajax.test-danit.com/api/json/posts').then(response => response.json())
    }
    removeLoader() {
        const loader = document.querySelector('.loader');
        loader.remove();
    }   
    addPost(){
        const addPost = document.querySelector('.add-post');
        addPost.addEventListener('click', (e) => {
            console.log('add post');
            const newPost = document.createElement('div');
            newPost.classList.add('new-post');
            const titlePost = document.createElement('h3');
            titlePost.innerText = "Уведіть заголовок";
            titlePost.style.textAlign = 'center';
            const textPost = document.createElement('h4');
            textPost.innerText = "Уведіть текст публікації";
            textPost.style.textAlign = 'center';
            const titleNewPost = document.createElement('input');
            titleNewPost.classList.add('title-new-post');
            const textNewPost = document.createElement('textarea');
            textNewPost.classList.add('text-new-post');
            const btnPush = document.createElement('button');
            btnPush.innerText = 'Надіслати';
            btnPush.classList.add('btn-push');
            document.body.prepend(newPost);
            newPost.append(titlePost, titleNewPost, textPost, textNewPost, btnPush);
            btnPush.addEventListener('click', (e) => {             
                const newTitle = document.querySelector('.title-new-post').value;
                const newText = document.querySelector('.text-new-post').value;
                console.log(newTitle, newText);
                newPost.remove();
                    name = "Leanne Graham";
                    username = "Bret";
                    email = "sincere@april.biz";
                    title = newTitle;
                    text = newText;

                (async() => {
                    const res = await fetch (`https://ajax.test-danit.com/api/json/posts`, {
                        method: 'POST',
                        body: JSON.stringify({
                            name,
                            username,
                            email,
                            title,
                            text
                          }),
                          headers: {
                            'Content-Type': 'application/json'
                          } 
                    }).then(response => console.log(response))
                    const addNewPost = new Card(name, username, email, title, text);
                    console.log(addNewPost);
                    addNewPost.renderCards()
                })();
            }); 
        });                                   
    }
}

const createPostsArr = async () => {
    const cardsArr = [];

    const api = new Api();
    const root = document.createElement('div');
    root.classList.add('root');
    document.body.append(root);
    api.addPost();  
    api.addLoader();
        await Promise.all([api.getUsers(), api.getPosts()]).then(data => {
            const [usersArray, postsArray] = data;
            usersArray.forEach(user => {
                postsArray.forEach(post => {
                    // console.log(post);
                    if (user.id === post.userId) {
                        name = user.name;
                        username = user.username;
                        email = user.email;
                        title = post.title;
                        text = post.body;
                        id = post.id;
                        const newCard = new Card(name, username, email, title, text, id);
                        cardsArr.push(newCard);
                        newCard.renderCards(data);
                    }
                });
            });
        });
        api.removeLoader(); 
        return cardsArr;
}

createPostsArr();  
