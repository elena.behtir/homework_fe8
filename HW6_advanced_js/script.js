async function getIp() {
    const ipGet = await fetch('https://api.ipify.org/?format=json', {method: "GET",})
    .catch(error => {
      console.log(error)
    });
    const ip = await ipGet.json();
    console.log(ip);

        const locationGet = await fetch(`http://ip-api.com/json/${ip.ip}?fields=status,message,continent,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,currency,isp,org,as,query`, {method: "GET",})
        .catch(error => {
          console.log(error)
        });
        const location = await locationGet.json();
        console.log(location);       
        const array = [location.country, location.regionName, location.region, location.city, location.lat, location.lon];
        console.log(array);
        getLocation(array);
}

function getLocation(array){
  const locationIP = document.createElement('div');
  document.body.append(locationIP);
  array.forEach(arr => {
    const getArr = document.createElement('div');
    getArr.innerText = arr;
    locationIP.appendChild(getArr);
  });
}

const btnIp = document.querySelector('.btn-ip');
btnIp.addEventListener('click', (e) => {
    getIp();
})
