function createGame() {
    const table = document.createElement("table"); // створення таблиці
    table.classList.add("table");
     for (let i = 0; i <= 9; i++) { // цикл створення рядків таблиці
        const tr = document.createElement("tr"); // створення рядка
        for (let j = 0; j <= 9; j++) { // цикл створення комірок
            const td = document.createElement("td"); // створення комірки
            tr.appendChild(td); // вставка комірки в рядок
        }
        table.appendChild(tr); // вставка рядка в таблицю
    }
    document.body.appendChild(table); // вставка таблиці в body
    i = Math.floor(Math.random() * 10); // створення рандомної координати комірки i
    j = Math.floor(Math.random() * 10); // створення рандомної координати комірки j
    console.log(i, j);
    let tdRandom = table.rows[j].children[i]; // вибір рандомної комірки 
    tdRandom.classList.add('mole'); // фарбування рандомної комірки в синій колір
    setTimeout (() => {
        // фабування виділеної рандомної комірки по кліку гравця в зелений колір
        if (tdRandom.classList.contains('mole')) {
            table.addEventListener('click', (e) => {
                tdRandom.classList.add('player'); // ?????? але чомусь не виходить пофарбувати в зелений?????
                resultPlayer++;
                console.log('player', resultPlayer);
                return resultPlayer;
            }); 
            // якщо гравець не встиг клікнути на синю комірку, то вона фарбується в червоний колір
            if (!(tdRandom.classList.contains('player') === true)) {
                tdRandom.classList.add('computer');
                resultComputer++;
                console.log('computer', resultComputer);
                return resultComputer;
            } 
        }
        tdRandom.classList.remove('mole');
    }, 1500);
}

let resultPlayer = 0;
let resultComputer = 0;

createGame();

