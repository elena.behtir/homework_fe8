import './App.css';
import { Button } from './Button.js';
import { Modal } from './Modal';
import React from 'react';


class App extends React.Component {

  state = { isModalVisibleFirst: false, isModalVisibleSecond: false }

  render() {
    return (
      <div className="App">

        <Button className="btn__main" text="Open first modal" 
        backgroundColor="brown"
        handleClick={(e) => {
          this.setState({isModalVisibleFirst: true})
        }} />

        <Button className="btn__main" text="Open second modal"
        backgroundColor="blueviolet"
        handleClick={(e) => {
          this.setState({isModalVisibleSecond: true})
        }} />
        
        {this.state.isModalVisibleFirst && (<Modal 
        header="Do you want delete this file?" 
        text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?" 
        backgroundColor = "#f64f4f"
        onClose={() => {this.setState({isModalVisibleFirst: false})}}
        actions={
          <>
          <Button className="btn__modal"
          text="Ok"
          backgroundColor="rgb(0, 0, 0, 0.3)"
          handleClick={(e) => {
            this.setState({isModalVisibleFirst: false})
            console.log("Файл видалено")
          }} />
          <Button className="btn__modal" 
          text="Cancel"
          backgroundColor="rgb(0, 0, 0, 0.3)"
          handleClick={(e) => {
            this.setState({isModalVisibleFirst: false})
          }} />
          </>}
         />)}

        {this.state.isModalVisibleSecond && (<Modal 
        header="Do you want change this file?" 
        text="Once you change this file, it won't be possible to undo this action. Are you sure you want to change it?" 
        backgroundColor = "#cc9df7"
        onClose={() => {this.setState({isModalVisibleSecond: false})}}
        actions={
          <>
          <Button className="btn__modal"
          text="Yes"
          backgroundColor="rgb(0, 0, 0, 0.3)"
          handleClick={(e) => {
            this.setState({isModalVisibleSecond: false})
            console.log("Файл змінено")
          }} />
          <Button className="btn__modal" 
          text="No"
          backgroundColor="rgb(0, 0, 0, 0.3)"
          handleClick={(e) => {
            this.setState({isModalVisibleSecond: false})
          }} />
          </>}
         />)}

      </div>
    );
  }
 
}

export default App;
