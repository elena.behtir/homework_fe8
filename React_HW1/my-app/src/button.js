import React from 'react'
import './button.scss';

export class Button extends React.Component {

    render() {       
        return <button className={`btn ${this.props.className}`}
            style={{backgroundColor: this.props.backgroundColor}}
            onClick={(e) => {
            this.props.handleClick()
        }}>{this.props.text}</button>     
    }
    
}
