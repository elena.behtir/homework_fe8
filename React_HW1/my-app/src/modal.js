import React from 'react'
import './modal.scss';

export class Modal extends React.Component {

    render() {
        return (
            <div className="wrapper"
            onClick={(e) => {
                this.props.onClose()
            }}>

                <div className="modal" 
                onClick={(e) => {
                    e.stopPropagation()
                }}
                style={{backgroundColor: this.props.backgroundColor}}>

                <h2 className="modal__header">{this.props.header}</h2>

                <div className="modal__close"
                onClick={(e) => {
                    this.props.onClose()
                }}>X</div>

                <p className="modal__text">{this.props.text}</p>

                <div>
                    {this.props.actions}
                </div>
            </div> 
        </div>
            
        )       
    }
}