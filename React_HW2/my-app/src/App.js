import './App.css';
import React from 'react';
import { Header } from './Components/Header';
import { Cards } from './Components/Cards';
import { Footer } from './Components/Footer';

class App extends React.Component {
  state = {
    cards: [],
    cardList: [],
    favoritList: []
  };

  componentDidMount() {
    fetch('./data/products.json')
      .then(r => r.json())
      .then(data => {
        this.setState({ cards: data });
        console.log("data", data);
      })
      .catch(error => console.log('error'));
    
      this.setState(() => {
        return { cardList: JSON.parse(localStorage.getItem('cardList')) || [] }
      })

      this.setState(() => {
        return { favoritList: JSON.parse(localStorage.getItem('favoritList')) || [] }
      })
  }

  cardListAdd = (product) => {
    // console.log(this)
    this.setState(({ cardList }) => {
      return { cardList: [...cardList, product] }
    }, () => {
      localStorage.setItem('cardList', JSON.stringify(this.state.cardList));
    })    
  }

  cardListSelect = (product) => {
    this.setState(({ favoritList }) => {
      return { favoritList: [...favoritList, product] }
    }, () => {
      localStorage.setItem('favoritList', JSON.stringify(this.state.favoritList));
    })  
  }

  render() {

    return (
      <div>

        <Header cardList={this.state.cardList} favoritList={this.state.favoritList} />

        <div className="cards">
          {this.state.cards.map((card) => (
            <Cards
              key={card.id}
              color={card.color}
              card={card}
              cardListAdd={this.cardListAdd}
              cardListSelect={this.cardListSelect}
            />
          ))}
        </div>

        <Footer />

      </div>
    );
  }

}

export default App;
