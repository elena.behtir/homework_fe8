import React from "react";
import PropTypes from "prop-types";
import './cards.scss';
import { Button } from './Button';
import { Modal } from './Modal';


export class Cards extends React.Component {

  state = { isModalAddCard: false, isModalSelectCard: false, isIconStarColor: false, isIconStarNoColor: true }

  render() {
    const { name, image, price, artcl } = this.props.card;
    return (
      <div className="card">
        <div className="card__name">{name}</div>
        <div className="card__wrap">
          <img className="card__img" src={image} alt="#" />
          <div className="card__info">
            <div className="card__info-price">Ціна: {price} грн.</div>
            <div>Артикул: {artcl}</div>
            <div>Колір: {this.props.color}</div>
          </div>
        </div>
        <div className="card__starsbtn">

          {this.state.isIconStarNoColor && <i className="fa-regular fa-star"
            onClick={(e) => this.setState({ isModalSelectCard: true })}
          ></i>}

          {this.state.isIconStarColor && <i className="fa-solid fa-star"></i>}

          <Button className="btn__main" text="Додати в кошик"
            backgroundColor="blueviolet"
            handleClick={(e) => {
              this.setState({ isModalAddCard: true })
            }} />
        </div>

        {this.state.isModalAddCard && (<Modal
          header="Додавання товару у кошик"
          text="Ви дійсно хочете додати цей товар у кошик?"
          backgroundColor="#cc9df7"
          onClose={() => { this.setState({ isModalAddCard: false }) }}
          actions={
            <>
              <Button className="btn__modal"
                text="Так"
                backgroundColor="rgb(0, 0, 0, 0.3)"
                handleClick={(e) => {
                  this.setState({ isModalAddCard: false })
                  this.props.cardListAdd(this.props.card)
                  console.log("Товар додано в кошик")
                }} />
              <Button className="btn__modal"
                text="Ні"
                backgroundColor="rgb(0, 0, 0, 0.3)"
                handleClick={(e) => {
                  this.setState({ isModalAddCard: false })
                }} />
            </>}
        />)}

        {this.state.isModalSelectCard && (<Modal
          header="Додавання товару в обране"
          text="Ви дійсно хочете додати цей товар в обране?"
          backgroundColor="#4f4ff9"
          onClose={() => { this.setState({ isModalSelectCard: false }) }}
          actions={
            <>
              <Button className="btn__modal"
                text="Так"
                backgroundColor="rgb(0, 0, 0, 0.3)"
                handleClick={(e) => {
                  this.setState({ isModalSelectCard: false })
                  this.setState({ isIconStarNoColor: false })
                  this.setState({ isIconStarColor: true })
                  this.props.cardListSelect(this.props.card)
                  console.log("Товар додано в обране")
                }} />
              <Button className="btn__modal"
                text="Ні"
                backgroundColor="rgb(0, 0, 0, 0.3)"
                handleClick={(e) => {
                  this.setState({ isModalSelectCard: false })
                }} />
            </>}
        />)}
      </div>
    )
  }
}

Cards.defaultProps = {
    color: 'в асортименті'
};


Cards.propTypes = {
  color: PropTypes.string,
  card: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    artcl: PropTypes.number.isRequired
  }).isRequired
}
