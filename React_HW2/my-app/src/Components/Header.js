import React from 'react'
import './header.scss';
import { Basket } from './Basket'
import { Select } from './Select'

export class Header extends React.Component {

    state = { isModalBasket: false, isModalSelect: false }

    render() {
        return (
            <div className="header">
                <img className="header__logo" src="./img/logo.jpg" alt="logo" />
                <h2 className="header__title">Інтернет-магазин "Оптичні прилади"</h2>
                <div>
                    <div className='header__icons'>
                    {this.state.isModalBasket && <Basket cardList={this.props.cardList}
                        onClose={() => {this.setState({isModalBasket: false})}} />}
                        <i className="fa-solid fa-basket-shopping icons-header" 
                            onClick={(e) => {
                                this.setState({isModalBasket: true})
                                const cardList = this.props.cardList
                                console.log(cardList)
                            }}
                        ></i>
                        <div>{this.props.cardList.length}</div>
                    </div>
                    
                    <div className='header__icons'>
                    {this.state.isModalSelect && <Select favoritList={this.props.favoritList}
                        onClose={() => {this.setState({isModalSelect: false})}} />}
                        <i className="fa-solid fa-star icons-header"
                            onClick={(e) => {
                                this.setState({isModalSelect: true})
                                const favoritList = this.props.favoritList
                                console.log(favoritList)
                            }}
                        ></i>
                        <div>{this.props.favoritList.length}</div>
                    </div>                    
                </div>
                
            </div>
            
        )       
    }
}