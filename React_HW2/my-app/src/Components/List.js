import React from 'react'
import './list.scss';

export class List extends React.Component {

    render() {
        const { name, image, price } = this.props.card;
        return (
            <div>
                <div className="list">                    
                    <img className="list__img" src={image} alt="#" />
                    <div className="list__name">{name}</div>        
                    <div className="list__info-price">{price} грн.
                    </div>          
                </div>
            </div>          
        )       
    }
}