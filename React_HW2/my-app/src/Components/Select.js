import React from 'react'
import './basket.scss';
import { List } from './List'

export class Select extends React.Component {

    render() {
        return (

            <div className="wrap-basket"
            onClick={(e) => {
                this.props.onClose()
            }}>
                <div className="basket"
                onClick={(e) => {
                    e.stopPropagation()}}
                >

                    <div className="list__close"
                    onClick={(e) => {
                        this.props.onClose()
                    }}>X</div>

                    <div className="basket__text">Обрані товари</div>

                        {this.props.favoritList.map((card) => (
                            <List 
                                key={card.id}
                                card={card}
                            />
                        ))}

                </div>  
            </div>          
        )       
    }
}