import './favorit-list.scss';


export function FavoritList(props) {

    const { name, image, price } = props.card;

    return (
        <div>
            <div className="favorit-list">
                <img className="favorit-list__img" src={image} alt="#" />
                <div className="favorit-list__name">{name}</div>
                <div className="favorit-list__info-price">{price} грн.</div>
            </div>
        </div>
    )
}
