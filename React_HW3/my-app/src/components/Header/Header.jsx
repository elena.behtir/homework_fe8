import React, { useState } from 'react'
import { NavLink, Link } from 'react-router-dom';
import './header.scss';
import { BasketPage } from '../../pages/BasketPage/BasketPage'
import { SelectPage } from '../../pages/SelectPage/SelectPage'

export const Header = (props) => {

    const [isModalBasket, setModalBasket] = useState(false);
    const [isModalSelect, setModalSelect] = useState(false);

    return (

        <div>
            <nav>
                <NavLink
                    to="/"
                    className=
                    {({ isActive }) =>
                        isActive ? 'active-link' : 'link'
                    }
                >
                    Головна сторінка
                </NavLink>
                <NavLink
                    to="/basket"
                    className={({ isActive }) =>
                        isActive ? 'active-link' : 'link'
                    }
                >
                    Кошик
                </NavLink>
                <NavLink
                    to="/select"
                    className={({ isActive }) =>
                        isActive ? 'active-link' : 'link'
                    }
                >
                    Обрані товари
                </NavLink>
            </nav>

            <div className="header">
                <img className="header__logo" src="./img/logo.jpg" alt="logo" />
                <h2 className="header__title">Інтернет-магазин "Оптичні прилади"</h2>
                <div>
                    <div className='header__icons'>
                        {isModalBasket && <BasketPage cardList={props.cardList}
                            totalCount={props.countTotalPrice()}
                            onClose={() => { setModalBasket(false) }} />}
                        <Link to="/basket">
                            <i className="fa-solid fa-basket-shopping icons-header"></i>
                        </Link>
                        <div>{props.cardList.length}</div>
                    </div>

                    <div className='header__icons'>
                        {isModalSelect && <SelectPage favoritList={props.favoritList}
                            onClose={() => { setModalSelect(false) }} />}
                        <Link to="/select">
                            <i className="fa-solid fa-star icons-header"></i>
                        </Link>
                        <div>{props.favoritList.length}</div>
                    </div>
                </div>

            </div>
        </div>
    )
}
