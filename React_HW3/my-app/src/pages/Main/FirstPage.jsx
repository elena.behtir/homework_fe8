import React, { useState, useEffect } from 'react';
import { Outlet } from 'react-router-dom';
import { Header } from '../../components/Header/Header';
import { Footer } from '../../components/Footer/Footer';

const savedFavorites = JSON.parse(localStorage.getItem('favoritList')) || []

export const FirstPage = () => {

  const [cards, setCards] = useState([]);
  const [cardList, setCardList] = useState([]);
  const [favoritList, setFavoritList] = useState(savedFavorites);


  useEffect(() => {
    fetch('./data/products.json', { method: 'GET' })
      .then(r => r.json())
      .then(data => {
        (setCards(data));
        console.log("data", data);
      })
      .catch(error => console.log('error'));
  }, [])

  useEffect(() => setCardList(JSON.parse(localStorage.getItem('cardList')) || []), [])

  useEffect(() => setFavoritList(JSON.parse(localStorage.getItem('favoritList')) || []), [])

  const cardListAdd = (card) => {
    const existingCartItem = cardList.find((item) => item.id === card.id);
    if (existingCartItem) {
      const updatedCartItems = cardList.map((item) => {
        if (item.id === card.id) {
          return {
            ...item,
            quantity: item.quantity + 1,
          };
        }
        return item;
      });
      setCardList(updatedCartItems);
    } else {
      const updatedCartItems = [...cardList, { ...card, quantity: 1 }];
      setCardList(updatedCartItems);
    }
  };


  const cardListDelete = (card) => {
    if (cardList.find(item => item.id === card.id)) {
      setCardList(prev => prev.filter(fav => fav.id !== card.id))
    }
  }

  useEffect(() => {
    localStorage.setItem('cardList', JSON.stringify([...cardList]));
  }, [cardList])


  const cardListSelect = (card) => {
    if (favoritList.find(item => item.id === card.id)) {
      setFavoritList(prev => prev.filter(fav => fav.id !== card.id))
    } else {
      setFavoritList(prev => [...prev, card])
    }
  }

  useEffect(() => {
    localStorage.setItem('favoritList', JSON.stringify([...favoritList]));
  }, [favoritList])


  return (

    <div>

      <Header cardList={cardList} favoritList={favoritList} />

      <Outlet context={{
        cards,
        setCards,
        cardList,
        setCardList,
        favoritList,
        setFavoritList,
        cardListAdd,
        cardListDelete,
        cardListSelect
      }} />

      <Footer />

    </div>
  );
}
