import React, { useState, useEffect } from "react";
import { useOutletContext } from 'react-router-dom';
import PropTypes from "prop-types";
import './main.scss';
import { Button } from '../../components/Button/Button';
import { Modal } from '../../components/Modal/Modal';


export function Main(props) {

  const [isModalAddCard, setModalAddCard] = useState(false);
  const [isModalSelectCard, setModalSelectCard] = useState(false);
  const [isIconStarColor, setIconStarColor] = useState(false ||
    localStorage.getItem(props.card.id) === 'true'
  );
  const [isIconStarNoColor, setIconStarNoColor] = useState(
    isIconStarColor ? false : true
  );

  useEffect(() => {
    localStorage.setItem(props.card.id, isIconStarColor);
  }, [isIconStarColor, props.card.id]);


  const { name, image, price, artcl } = props.card;

  const { cardListAdd, cardListSelect } = useOutletContext();

  return (
    <div className="card">
      <div className="card__name">{name}</div>
      <div className="card__wrap">
        <img className="card__img" src={image} alt="#" />
        <div className="card__info">
          <div className="card__info-price">Ціна: {price} грн.</div>
          <div>Артикул: {artcl}</div>
          <div>Колір: {props.color}</div>
        </div>
      </div>
      <div className="card__starsbtn">

        {isIconStarNoColor && <i className="fa-regular fa-star"
          onClick={(e) => (setModalSelectCard(true))}
        ></i>}

        {isIconStarColor && <i className="fa-solid fa-star"
          onClick={(e) => {
            setIconStarNoColor(true)
            setIconStarColor(false)
            cardListSelect(props.card)
            console.log("товар видалено в обраного")
          }}
        ></i>}


        <Button className="btn__main" text="Додати в кошик"
          backgroundColor="blueviolet"
          handleClick={(e) => {
            setModalAddCard(true)
          }} />
      </div>

      {isModalAddCard && (<Modal
        header="Додавання товару у кошик"
        text="Ви дійсно хочете додати цей товар у кошик?"
        backgroundColor="#cc9df7"
        onClose={() => { setModalAddCard(false) }}
        actions={
          <>
            <Button className="btn__modal"
              text="Так"
              backgroundColor="rgb(0, 0, 0, 0.3)"
              handleClick={(e) => {
                setModalAddCard(false)
                cardListAdd(props.card)
                console.log("Товар додано в кошик")
              }} />
            <Button className="btn__modal"
              text="Ні"
              backgroundColor="rgb(0, 0, 0, 0.3)"
              handleClick={(e) => {
                setModalAddCard(false)
              }} />
          </>}
      />)}

      {isModalSelectCard && (<Modal
        header="Додавання товару в обране"
        text="Ви дійсно хочете додати цей товар в обране?"
        backgroundColor="#4f4ff9"
        onClose={() => { (setModalSelectCard(false)) }}
        actions={
          <>
            <Button className="btn__modal"
              text="Так"
              backgroundColor="rgb(0, 0, 0, 0.3)"
              handleClick={(e) => {
                setModalSelectCard(false)
                setIconStarNoColor(false)
                setIconStarColor(true)
                cardListSelect(props.card)
                console.log("Товар додано в обране")
              }} />



            <Button className="btn__modal"
              text="Ні"
              backgroundColor="rgb(0, 0, 0, 0.3)"
              handleClick={(e) => {
                setModalSelectCard(false)
              }} />
          </>}
      />)}


    </div>
  )
}


Main.defaultProps = {
  color: 'в асортименті'
};


Main.propTypes = {
  color: PropTypes.string,
  card: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    artcl: PropTypes.number.isRequired
  }).isRequired
}

