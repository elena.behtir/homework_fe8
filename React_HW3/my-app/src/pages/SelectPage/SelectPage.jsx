import React from 'react';
import { useOutletContext } from 'react-router-dom'
import './select.scss';
import { FavoritList } from '../../components/FavoritList/FavoritList'

export function SelectPage() {

    const { favoritList } = useOutletContext();

    return (

        <div className='main'>
            <div className='select'>

                <div className="select__text">Обрані товари</div>

                {favoritList.map((card) => (
                    <FavoritList
                        key={card.id}
                        card={card}
                    />
                ))}

            </div>
        </div>



    )
}
