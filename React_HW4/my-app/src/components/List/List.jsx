import React, { useState } from 'react';
import { useOutletContext } from 'react-router-dom';
import './list.scss';
import { Modal } from '../../components/Modal/Modal';
import { Button } from '../../components/Button/Button';

export function List(props) {

    const { name, image, price, quantity } = props.card;

    const [isModalDeleteCard, setModalDeleteCard] = useState(false);

    const { cardListDelete } = useOutletContext();

    return (
        <div>
            <div className="list">
                <img className="list__img" src={image} alt="#" />
                <div className="list__name">{name}</div>
                <div>Кількість {quantity} шт.</div>
                <div className="list__info-price">{price} грн.</div>
                <i className="fa-regular fa-circle-xmark list__delete"
                    onClick={(e) => (setModalDeleteCard(true))}
                ></i>


            </div>

            {isModalDeleteCard && (<Modal
                header="Видалення товару з кошика"
                text="Ви дійсно хочете видалити цей товар з кошика?"
                backgroundColor="#f64f4f"
                onClose={() => { setModalDeleteCard(false) }}
                actions={
                    <>
                        <Button className="btn__modal"
                            text="Так"
                            backgroundColor="rgb(0, 0, 0, 0.3)"
                            handleClick={(e) => {
                                setModalDeleteCard(false)
                                cardListDelete(props.card)
                                console.log("Товар видалено з кошика")
                            }} />
                        <Button className="btn__modal"
                            text="Ні"
                            backgroundColor="rgb(0, 0, 0, 0.3)"
                            handleClick={(e) => {
                                setModalDeleteCard(false)
                            }} />
                    </>}
            />)}
        </div>
    )
}
