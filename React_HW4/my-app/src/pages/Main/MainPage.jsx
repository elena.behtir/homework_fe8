import React from 'react';
import { useOutletContext } from 'react-router-dom';
import { Main } from './Main';


export function MainPage() {

    const { cards, cardListAdd, cardListSelect } = useOutletContext();


    
    return (
        <div className="cards">
            {cards.map((card) => (
                <Main
                    key={card.id}
                    color={card.color}
                    card={card}
                    cardListAdd={cardListAdd}
                    cardListSelect={cardListSelect}
                  />
            ))}
        </div>
    )

}
