import React from 'react';
import './select.scss';
import { FavoritList } from '../../components/FavoritList/FavoritList';
import { favoritListSelector } from '../../redux/selector';
import { useDispatch, useSelector } from 'react-redux';


export function SelectPage() {

    // const { favoritList } = useOutletContext();
    const favoritList = useSelector(favoritListSelector);
    // const dispatch = useDispatch();

    return (

        <div className='main'>
            <div className='select'>

                <div className="select__text">Обрані товари</div>

                {favoritList.map((card) => (
                    <FavoritList
                        key={card.id}
                        card={card}
                    />
                ))}

            </div>
        </div>



    )
}
