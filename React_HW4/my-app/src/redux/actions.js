// actions for SelectPage (favoritList)
export const ADD_PRODUCT_TO_FAVORITLIST_ACTION_TYPE = 'favoritList/addProduct';
export const REMOVE_PRODUCT_FROM_FAVORITLIST_ACTION_TYPE = 'favoritList/removeProduct';
export const TOGGLE_PRODUCT_TO_FAVORITLIST_ACTION_TYPE = 'favoritList/toggleProduct';

// actons for BasketPage (cardList)
export const ADD_PRODUCT_TO_CARDLIST_ACTION_TYPE = 'cardList/addProduct';
export const REMOVE_PRODUCT_FROM_CARDLIST_ACTION_TYPE = 'cardList/removeProduct';

// actions for MainPage (cards)
export const ADD_CARD_ACTION_TYPE = 'cards/addCard';