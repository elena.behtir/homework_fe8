import { ADD_PRODUCT_TO_FAVORITLIST_ACTION_TYPE,
     REMOVE_PRODUCT_FROM_FAVORITLIST_ACTION_TYPE, 
     TOGGLE_PRODUCT_TO_FAVORITLIST_ACTION_TYPE } from "../actions"

     
// Редюсер для обраного

export function favoritListReducer(state = {}, action) {
    switch (action.type) {
      case ADD_PRODUCT_TO_FAVORITLIST_ACTION_TYPE:
        return [...state, action.payload.card]
      case REMOVE_PRODUCT_FROM_FAVORITLIST_ACTION_TYPE:
        return state.filter(fav => fav.id !== action.payload.card.id)
      case TOGGLE_PRODUCT_TO_FAVORITLIST_ACTION_TYPE:     
        return state.find(item => item.id === action.payload.card.id) ? state.filter(fav => fav.id !== action.payload.card.id) : [...state, action.payload.card]
      default:
        return state
    }
  }