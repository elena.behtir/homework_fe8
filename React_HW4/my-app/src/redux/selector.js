export const favoritListSelector = state => state.favoritList;
export const cardListSelector = state => state.cardList;
export const cardsSelector = state => state.cards;