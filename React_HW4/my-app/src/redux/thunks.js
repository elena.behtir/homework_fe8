export function getCards() {
    return (dispatch) => {
        dispatch({ type: 'GET_CARD_ACTION_TYPE' })
        fetch('./data/products.json', { method: 'GET' })
        .then(result => result.json())
        .then(data => {
            dispatch({ type: 'GET_CARD_SUCCESS',
            payload: { cards: data} })
        })
    }
}
