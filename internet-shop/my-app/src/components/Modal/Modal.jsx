import React from 'react'
import './modal.scss';

export function Modal(props) {

    return (
        <div className="wrapper"
            onClick={(e) => {
                props.onClose()
            }}>

            <div className="modal"
                onClick={(e) => {
                    e.stopPropagation()
                }}
                style={{ backgroundColor: props.backgroundColor }}>

                <h2 className="modal__header">{props.header}</h2>

                <div className="modal__close"
                    onClick={(e) => {
                        props.onClose()
                    }}>X</div>

                <p className="modal__text">{props.text}</p>

                <div>
                    {props.actions}
                </div>
            </div>
        </div>

    )
}
